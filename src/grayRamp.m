rows = 2160;
cols = 3840; % 4K Resolution
grayRamp = uint16(linspace(0,65535,1024)); %1024 Bars
grayRamp = repmat(grayRamp, [rows, 1]); %Make it 2160 Wide
grayRamp = imresize(grayRamp,[2160 3840],"nearest"); % Reshape
%to approximately 3840 using NearestNeighbours for step size
% Uncomment to add noise
%grayRamp = uint16(round(256*randn(size(grayRamp))) + ...
%  double(grayRamp)); % Do this only for adding noise
% Adjust the width based on maximum HDR area
grayRamp(1 : 1000, :) = 0; % Remove first 1000 pixels from top
grayRamp(1200 : end , :) = 0; % Remove pixels from 1200,
%making only 200 pixels with data
imshow(grayRamp); % Show image
% uncomment to write the image
%imwrite(grayRamp,"grayRampWithNoise.tiff");