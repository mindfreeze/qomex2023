frameSize = [2160, 4096]; % 4K resolution
aspect_ratio = [9, 17];
percent = 1;
center_flag = 1;
floor_flag = 0;
noaspect_flag = 1;
random_shuffle = 1;

img_data = getBox(frameSize, aspect_ratio, this_percent, ...
  center_flag, floor_flag, noaspect_flag,random_shuffle);
imshow(img_data);
file_name = sprintf(...
  'boxes/4096x2160/%s_center_%d_percent_%d_strict_%d_ar_%s_shuffle_%d.tiff',...
  file_prefix,center_flag, this_percent,floor_flag, ar_val,random_shuffle);
imwrite(img_data,file_name); % Save as

function [img] = getBox(frameSize, aspect_ratio, percent,...
                        center, floor_state, noaspect_flag,random_shuffle)
% Generates Test patterns:
% frameSize - Target Resolution [Height, Width].
% aspectRatio - Target Aspect Ratio [Height, Width].
% percent - Percent of white pixels to be used.
% center - Flag to indicate where the box to be placed
% floor_state - Flag to indicate where you want to be always under that percent
%              or you want to have closest to the percent 
%              for the given resolution@aspectratio. 
% noaspect_flag - Flag to indicate that we do not care about aspect ratio.
% random_shuffle - Flag to shuffle the white pixels across the image, to
%                 generate starlight pattern.

window_size = percent/100;
rectWidth = floor(frameSize(2) * window_size);
rectHeight = floor(frameSize(1) * window_size);
image_area = (frameSize(1)*frameSize(2));
box_area  = floor(((image_area)*window_size));
estimatedArea = 0;
while estimatedArea < box_area
  if noaspect_flag == 1
    rectHeight = rectHeight + 1;
    rectWidth = rectWidth  + 1;
    estimatedArea = rectWidth * rectHeight;
  else
    rectHeight = rectHeight + 1;
    height_ar = rectHeight/aspect_ratio(1);
    rectWidth = floor(height_ar * aspect_ratio(2));
    estimatedArea = rectWidth * rectHeight;
  end
end
if floor_state == 1
  rectHeight = rectHeight - 1;
  height_ar = rectHeight/aspect_ratio(1);
  rectWidth = floor(height_ar * aspect_ratio(2));
  estimatedArea = rectWidth * rectHeight;
end
startX = round((frameSize(2) - rectWidth) / 2);
startY = round((frameSize(1) - rectHeight) / 2);

if center ~= 1
  startX = 1;
  startY = 1;
end
if percent ==100
  rectHeight = frameSize(1);
  rectWidth = frameSize(2);
end

img = zeros(frameSize, 'uint16');
img(startY:startY+rectHeight-1, startX:startX+rectWidth-1) = intmax('uint16');
if random_shuffle == 1
  img = zeros(frameSize, 'uint16');
  idx = randperm(image_area, estimatedArea);
  img(idx) = intmax('uint16');
end
this_dim = size(img);
line_to_print = sprintf("\nArea:%dx%d@%0.4f for %d:%d@%d window;size:%dx%d",...
  rectWidth,rectHeight, ...
  ((estimatedArea/image_area)*100),aspect_ratio(2), aspect_ratio(1), ...
  percent, this_dim(1), this_dim(2));
fprintf(line_to_print);

end