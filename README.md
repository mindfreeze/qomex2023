# QoMEX 2023 Supplementary Material

## Recommendations for Verifying HDR Subjective Testing Workflows

Vibhoothi$^{\dagger}$, Angeliki Katsenou$^{\dagger,\ddagger}$,John
Squires$^{\dagger}$, François Pitié$^{\dagger}$, Anil Kokaram$^{\dagger}$

$^{\dagger}$ Sigmedia Group, Department of Electronic and Electrical
Engineering, *Trinity College Dublin*, Ireland

$^{\ddagger}$ Department of Electrical and Electronic Engineering, University of Bristol, United Kingdom

$^{\dagger}$\{vibhoothi, john.squires, pitief, anil.kokaram\}at tcd dot ie

$^{\ddagger}$ \{angeliki.katsenou\} at bristol dot ac dot uk


Conference Homepage: [QoMEX 2023
](https://sites.google.com/view/qomex2023/home?authuser=0)

ArXiv Pre-print: [doi.org/10.48550/arXiv.2305.11858](https://arxiv.org/abs/2305.11858)

## Table of Contents
- [QoMEX 2023 Supplementary Material](#qomex-2023-supplementary-material)
  - [Recommendations for Verifying HDR Subjective Testing Workflows](#recommendations-for-verifying-hdr-subjective-testing-workflows)
  - [Table of Contents](#table-of-contents)
  - [Abstract](#abstract)
  - [Workflow](#workflow)
  - [Test Patterns](#test-patterns)
    - [Night-sky test pattern](#night-sky-test-pattern)
    - [White Window](#white-window)
    - [Gray Ramp](#gray-ramp)
    - [Reference code](#reference-code)
  - [Reference Images](#reference-images)
  - [STeM2 Conversions](#stem2-conversions)
  - [Usage of Test Materials](#usage-of-test-materials)
  - [Reference](#reference)

## Abstract

Over the past few years, there has been an increase
in the demand and availability of High Dynamic Range (HDR) displays and content.
To ensure the production of high-quality materials, human evaluation is
required. However, ascertaining
whether the full playback pipeline is indeed HDR-compliant can be challenging.
In this paper, we present a set of recommendations for conformance testing to
validate various aspects of the testing
workflow, including playback, displays, brightness, colours, and
viewing environment. We assessed the effectiveness of HDR
conversion techniques used in current standards development
(3GPP) for making source materials. Additionally, we evaluate
HDR display technologies, including OLED and LCD, using both
consumer television and a reference monitor.


## Workflow

![img](img/workflow-diagram-v3.png)

A typical playback pipeline for HDR testing. Input video inside the workstation
denotes the final video after conversions.


## Test Patterns

### Night-sky test pattern
This is a synthetic test pattern simulating a night-sky pattern.
The amount of pixels determines the intensity of pixels (or starlight).
The quality of the panel depends on how the target panel behaves with different levels
of starlight.

Can be used to measure, a) the quality of the panel, b) Display blooming
effects, and c) Local dimming.

A good panel illuminates only the desired pixels and does not increase/decrease
brightness based on the percent of white/black pixels in the screen.

### White Window

This is used to assess the brightness behavior based on the target display
resolution. Can be used with/without respecting the aspect ratio.

A good panel tries to sustain peak brightness for longer periods for smaller
window-size and tries to keep the peak brightness for ~10% window as per the
panels available in the current market (as of Q2 2023).

### Gray Ramp

This is used to validate 10bit for the playback pipeline.


### Reference code

Reference Matlab code for these is available in
`patternMaker`.m`](src/patternMaker.m), and [`grayRamp.m`](src/grayRamp.m).

*How to use?*

Define the target variables like Resolution, aspect ratio, location of image, and other specifications.
For generating a starlight pattern, keep `random_shuffle` as
1.

*Example*
```matlab
frameSize = [2160, 4096]; % 4K resolution
aspect_ratio = [9, 17];
percent = 1;
center_flag = 1;
floor_flag = 0;
noaspect_flag = 1;
random_shuffle = 1;

img_data = getBox(frameSize, aspect_ratio, this_percent, ...
  center_flag, floor_flag, noaspect_flag,random_shuffle);
imshow(img_data);

getBox(frameSize, aspect_ratio, percent,...
                        center, floor_state, noaspect_flag,random_shuffle)
% Generates Test patterns:
% frameSize - Target Resolution [Height, Width].
% aspectRatio - Target Aspect Ratio [Height, Width].
% percent - Percent of white pixels to be used.
% center - Flag to indicate where the box to be placed
% floor_state - Flag to indicate where you want to be always under that percent
%              or you want to have closest to the percent 
%              for the given resolution@aspectratio. 
% noaspect_flag - Flag to indicate that we do not care about aspect ratio.
% random_shuffle - Flag to shuffle the white pixels across the image, to
%                 generate starlight pattern.
```

## Reference Images
Sample images for the 4096x2160 images are available in the
[test-pattern](/test-pattern) folder which is at 17:9 aspect ratio. The
images are in TIFF formats.
There are 5 patterns, they are,
+ `center_0_strict_0_ar_179_shuffle_0`: Not centered, the target image is nearest value to the target percentages, Aspect ratio of 17:9.
+ `center_0_strict_0_ar_179_shuffle_1`: Starlight pattern at 17:9 ratio.
+ `center_1_strict_0_ar_00_shuffle_0`: Centered, the target image is nearest value to the target percentages, no aspect ratio.
+ `center_1_strict_0_ar_179_shuffle_0`: Centered, the target image is nearest value to the target percentages, Aspect ratio of 17:9.
+ `center_1_strict_1_ar_179_shuffle_0`: Not centered, the target image white-window is always under the target percentages, Aspect ratio of 17:9.
+ `grayRamp.tiff`: Gray Ramp of 1024 levels
+ `grayRampWithNoise.tiff`: Gray Ramp of 1024 levels with small level of noise.


| ![](/img/grayRamp.tiff.png) | ![](/img/grayRampWithNoise.tiff.png)| ![](/img/WhiteBox_4096x2160_center_0_percent_2_strict_0_ar_179_shuffle_0.tiff.png) |
|    :-:    |   :-:      |   :-:     |
| ![](/img/WhiteBox_4096x2160_center_0_percent_2_strict_0_ar_179_shuffle_1.tiff.png) | ![](/img/WhiteBox_4096x2160_center_1_percent_2_strict_0_ar_00_shuffle_0.tiff.png) | ![](/img/WhiteBox_4096x2160_center_1_percent_2_strict_0_ar_179_shuffle_0.tiff.png) |
| | ![](/img/WhiteBox_4096x2160_center_1_percent_2_strict_1_ar_179_shuffle_0.tiff.png) |
|||


## STeM2 Conversions
To validate the HDR intermediate conversion we used the latest [American Society
of Cinematographers's Standard Evaluation Material
2](https://dpel.aswf.io/asc-stem2/). We used the lossless HDR App 2E IMF files.
These are at Cinematic 4K Resolution. The conversion of IMF to TIFF was through
DaVinciResolve. The conversion of TIFF to YUV@420 is via HDRTools. The HDRTools
cfg file is [HDRConvert420.cfg](src/HDRConvert420.cfg).

[av-scenechange](https://github.com/rust-av/av-scenechange) is used to detect the scenchanges for making the small shots for
testing the encoder.

## Usage of Test Materials
The HDR test patterns are released under CC-NC-ND-4.0, please retain the
[copyright notice](/test-pattern/COPYRIGHT.txt) on usage and redistribution.

```
Digital Video Sequence "HDR Test materials"
Copyright (C)  Sigmedia Group - 2023
Copyright (C)  Trinity College Dublin - 2023

4096x2160, TIFF sequences

COPYRIGHT AND LICENSE INFORMATION
June 2023

Sigmedia Group,
Department of Electronic and Electrical Engineering,
Trinity College Dublin, Ireland


The image test sequence provided above and all intellectual property rights therein
remain the property of Trinity College Dublin. This video sequence is licensed
under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-nc-nd/4.0/
```
## Reference

[1]: Vibhoothi, F. Pitié, A. Katsenou, and A. Kokaram, “4K HDR video with AV1: A Reality
    Check”, FOSDEM 2023, https://fosdem.org/2023/schedule/track/open_media/